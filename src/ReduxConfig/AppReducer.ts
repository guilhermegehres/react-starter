import {combineReducers} from 'redux';
import { Reducer } from './../Modules/HelloModule/Redux/Reducer';
import { Reducer as ReducerFlight } from './../Modules/Flight/Redux/Reducer';

export const AppReducer = combineReducers({
    ReducerFlight
});
