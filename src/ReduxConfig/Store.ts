import { createStore, applyMiddleware } from 'redux';
import { AppReducer } from './AppReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

const middleware : any[] = [thunk]
export default createStore(
    AppReducer,
    composeWithDevTools (
        applyMiddleware(...middleware)
    )
);