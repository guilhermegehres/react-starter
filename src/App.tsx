import React, { Component } from 'react';
import './App.scss';
import { RouterModule, Route, AppRouter } from './RouterModule';
import { HelloModule } from './Modules/HelloModule/HelloModule';
import { FlightModule } from './Modules/Flight/FlightModule';

export class App extends Component implements RouterModule {
    routes : Array<Route> = [
        {path : "/", component: FlightModule},
    ];

  render() {
    return <AppRouter isRoot={true} routes={this.routes} match="" />
  }
}