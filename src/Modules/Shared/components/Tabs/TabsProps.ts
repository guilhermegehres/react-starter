export interface TabsProps{
    listTab: Tab[];
}

export interface TabsState{
    selectedTab: Tab;
}

export interface Tab { 
    name: string, 
    title: string, 
    component: any 
}
