import React, { Component } from 'react'
import './Tabs.scss'
import { TabsState, TabsProps, Tab } from './TabsProps';

export class Tabs extends Component<TabsProps, TabsState>{
    constructor(props: TabsProps) {
        super(props);
        this.state = {
            selectedTab: props.listTab[0]
        }
    }

    setSelectedTab = (tab: Tab) => {
        this.setState({ ...this.state, selectedTab: tab })
    }

    render() {
        return <div className="Tabs">
            <ul className="nav nav-tabs">
                {this.props.listTab.map((tab, index) => {
                    const active = this.state.selectedTab.name == tab.name ? "active" : "";
                    return <li className="nav-item" key={index}>
                        <span className={"nav-link pointer " + active} onClick={() => this.setSelectedTab(tab)}>{tab.title}</span>
                    </li>
                })}
            </ul>
            <div className="base-nav-container">
                {this.state.selectedTab.component}
            </div>
        </div>
    }
}