import React, { Component } from "react";
import "./StyledFormikField.scss"
import { Field } from "formik";

export class StyledFormikField extends Component<any>{
    render(){
        return <Field className="StyledFormikField" {...this.props}/>
    }
}