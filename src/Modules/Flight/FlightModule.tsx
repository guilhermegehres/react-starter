import React, { Component } from 'react';
import { RouterModule, Route, AppRouter } from '../../RouterModule';
import { withRouter, RouteComponentProps } from 'react-router';
import { AddFlight } from './Pages/AddFlight/AddFlight';

class FlightModuleClass extends Component<RouteComponentProps> implements RouterModule {
    routes : Array<Route> = [
        {path : "", component: AddFlight}
    ];

  render() {
    return <AppRouter routes={this.routes} match={this.props.match.path} />
  }
}

export const FlightModule = withRouter(FlightModuleClass);