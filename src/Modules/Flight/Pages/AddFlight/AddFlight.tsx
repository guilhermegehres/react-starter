import React,  { Component } from "react";
import { Tabs } from "../../../Shared/components/Tabs/Tabs";
import { AddFlightTab } from "../../Components/AddFlightTab/AddFlightTab";

export class AddFlight extends Component{
    render(){
        return <div className="page">
            <div className="container">
                <Tabs 
                    listTab={[
                        {title: "Add flight", name: 'add', component: <AddFlightTab />},
                        {title: "Upload flights", name: 'upload', component: <AddFlightTab />},
                        {title: "Enter co2 emission", name: 'emission', component: <AddFlightTab />},
                    ]}
                />
            </div>
        </div>
    }
}