import React, { Component } from "react";
import { Formik, Form  } from "formik";
import { StyledFormikField } from "../../../Shared/components/StyledFormikField/StyledFormikField";
export class AddFlightForm extends Component{
    render(){
        return <Formik 
            initialValues={{name: "NAMO"}}
            onSubmit={values => console.log(values)}
            render={() => {
                return <Form>
                    <StyledFormikField name="name"/>
                    <button type="submit">SAVE</button>
                </Form>
            }}
        />
    }
}