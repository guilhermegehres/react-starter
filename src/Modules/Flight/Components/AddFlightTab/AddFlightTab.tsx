import React,  { Component } from "react";
import { AddFlightForm } from "../../Forms/AddFlightForm/AddFlightForm";

export class AddFlightTab extends Component{
    render(){
        return <AddFlightForm />
    }
}