import React, { Component } from "react";
import { IHelloPage } from "./IHelloPage";

export class HelloPage extends Component<IHelloPage>{
    render(){
        return <div>HELLO PAGE</div>
    }
}