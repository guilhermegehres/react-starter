import React, { Component } from 'react';
import { RouterModule, Route, AppRouter } from './../../RouterModule';
import { HelloPage } from './Pages/HelloPage/HelloPage';
import { withRouter, RouteComponentProps } from 'react-router';

class HelloModuleClass extends Component<RouteComponentProps> implements RouterModule {
    routes : Array<Route> = [
        {path : "", component: HelloPage}
    ];

  render() {
    return <AppRouter routes={this.routes} match={this.props.match.path} />
  }
}

export const HelloModule = withRouter(HelloModuleClass);