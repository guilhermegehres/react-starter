import {
    Types
} from './Types';
import { ReduxInterface } from './ReduxInterface';
const initialState: ReduxInterface = {
    anything: undefined
};

export const Reducer = (state = initialState, action: any): ReduxInterface => {
    switch (action.type) {
        case Types.SET_ANYTHING: {
            return <ReduxInterface>{
                ...state,
                anything: action.payload
            };
        }
        
        default : {
            return state;
        }
    }
}