export class TypesClass{
    prefix = "REDUX_NAME__";
    SET_ANYTHING = this.prefix + "SET_ANYTHING";
}

export const Types = new TypesClass();
