
1. Divisão das pastas do projeto
    * 1.1 - Interfaces : Pasta para interfaces gerais do sistema;
    * 1.2 - Model : Pasta com o modelos de dados feito com interfaces;
    * 1.3 - Modules : Pasta com os modulos da aplicação
        * 1.3.1: Module.tsx
            Cada pasta dentro da pasta modules é um módulo. 
            Cada módulo deve conter um arquivo chamado Module.tsx que é onde estão as sub rotas do modulo.
            O arquivo Module.tsx deve exportar uma classe semelhante a classe abaixo
            ```jsx
                import React, { Component } from 'react';
                import { withRouter } from 'react-router';
                import { connect } from 'react-redux';
                import { RouterModule, Route, AppRouter } from './../../RouterModule';


                class ModuleClass extends Component<any> implements RouterModule {

                    routes : Array<Route> = [
                        {path : "/", isAuth : true, exact: true, component : ModuleList},
                        {path : "/:id", isAuth : true, exact: true, component : ModuleDetail}
                    ];

                    render() {
                        return (
                            <AppRouter match={this.props.match.path} routes={this.routes} />
                        );
                    }
                }

                const mapStateToProps = (state : any, ownProps:  any) => ({})

                export const Module = withRouter(connect(
                    mapStateToProps,
                    {}
                )(ModuleClass))
            ```
        * 1.3.2: Estrutura de pastas de um módulo
            Os modulos devem conter as seguintes pastas:
            - components : Deve conter os components compartilhados do módulo. Caso o component seja usado em mais de um módulo deve ser movido para a pasta raíz **shared**
            - interfaces : Deve conter as interfaces relativas apenas aquele módulo. Caso a interface seja usada em mais de um módulo deve ser movido para a pasta raíz **interfaces**
            - pages : Deve conter components que representam as página do módulo. O arquivo Module.tsx do módulo deve importar para cada rota um component relativo a página ou outro Module.tsx de outro modulo.
            - redux : Pasta com redux do módulo. Os arquivos dessa pasta seguem o padrão abaixo :
                1. actions.ts : Contem as actions desse redux. Deve ter o formato a seguir :
                 ```javascript 
                    import { Action } from "../../../interfaces/ReduxAction";
                    import { ModuleTypes } from "./types";
                    export const ModuleActions = {
                        action(data : Data) : Action{
                            return {
                                type : LoginTypes.TYPE,
                                payload : {...data}
                            }
                        }
                    }
                 ```
                2. reducer.ts : Contem reducer do módulo. Deve ter o formato a seguir :
                 ```javascript 
                    import {
                        ModuleTypes
                    } from './types';
                    import { Action } from '../../../interfaces/ReduxAction';
                    import { ReduxInterface } from './redux-interface';

                    const initialState : ReduxInterface = {
                        data: {}
                    };

                    export const moduleReducer = (state = initialState, action : Action) => {
                        switch (action.type) {
                            case ModuleTypes.TYPE:{
                                return {
                                    ...state,
                                    data: {...action.payload},
                                };
                            }
                            default:
                                return state;
                        }
                    }
                 ```
                3. redux-interface.ts : Contem a interafce desse redux. Deve ter o formato a seguir :
                 ```javascript 
                    export interface ReduxInterface{
                        data : any;
                    }
                 ```
                4. types.ts : Contem os types desse redux. Deve ter o formato a seguir :
                 ```javascript 
                    import { ReduxType } from "../../../interfaces/ReduxTypes";
                    class ModuleTypesClass implements ReduxType{
                        prefix = "MODULE__";
                        LOGITYPEN = this.prefix + "TYPE";
                    }
                    export const ModuleTypes = new ModuleTypesClass();
                 ```
            - services : Pasta com services de acesso a api. Cada service deve manter o padrão abaixo : 
                 ```javascript 
                    import { http, httpTransformResponse } from "../../../utils/Http";

                    export const ModuleService = {
                        apiRestGet : async () : Promise<Data> => {
                            return httpTransformResponse(await http.get("/my/endpoint"));
                        }
                    }
                 ```
    * 1.4 - ReduxConfig : Pasta redux da aplicação. 
        Contem o arquivo AppReducers.ts que é onde são registrados os reducers de cada módulo dentro da função combineReducers({moduleReducer}).
        Contém o arquivo Store.ts que é onde é criado a store do redux. Normalmente esse arquivo nunca será alterado.
    * 1.5 - Shared : Pasta com componentes compartilhados entre os módulos
    * 1.6 - Utils : Qualquer script, const, class, function, etc que seja compartilhado entre a aplicação toda
    * 1.7 - App.ts : Módulo raíz da aplicação. Igual a um módulo explicado anteriormente. As rotas desse módulo devem sempre chamar outros módulos e nunca components como ocorre com as páginas de outros módulos
    * 1.8 - Demais arquivos/pastas não devem ser alterados.

2. Como criar um component
    * 2.1 Todo component deve ser camel case, Ex: MyComponent
    * 2.2 Arquivos da pasta de um componente 
        - MyComponent.tsx : Arquivo principal de um component. Deve manter o mesmo nome da pasta e deve exportar um component com o mesmo nome da pasta.
            Caso o componente não tenha nenhum HoC fazendo wrapping deve ser declarado como abaixo:
            ```javascript
            import React, { Component } from 'react';
            import "./MyComponent.scss";
            import { IMyComponent, MyComponentState, MyComponentExposedProps } from './IMyComponent';
            import { connect } from 'react-redux';
            export class MyComponent extends Component<IMyComponent, MyComponentState>{
                render(){
                    return <div></div>
                }
            }
            ```
            Caso o componente tenha HoC fazendo wrapping deve ser declarado como abaixo. Note que para funcionar o autocomplete das props na hora de usar o componente deve ter um casting para o tipo ComponentClass do react passando
            o tipo genérico importado do arquivo props(Será abodado a seguir).
            ```javascript
            import React, { Component } from 'react';
            import "./MyComponent.scss";
            import { IMyComponent, MyComponentState } from './TreeProps';
            import { connect } from 'react-redux';
            class MyComponentClass extends Component<IMyComponent, MyComponentState>{
                render(){
                    return <div></div>
                }
            }

            const mapStateToProps = (state: any, ownProps: any) => ({
                data : state.moduleReducer.data
            })

            export const MyComponent = connect(
                mapStateToProps,
                {}
            )(MyComponentClass) as ComponentClass<MyComponentExposedProps>;
            ```
        - IMyComponent.ts : Arquivo que exporta interfaces de props, exposedProps e state do componente
           O nome da interface de props deve ter o seguinte formato IMyComponent e deve ser usada no tipo genérico Component<IMyComponent> na hora da criação do component;
           O nome da interface de state deve ter o seguinte formato MyComponentState e deve ser usada no tipo genérico Component<IMyComponent, MyComponentState> na hora da criação do component;
           O nome da interface de exposedProps deve ter o seguinte formato MyComponentExposedProps e deve ser usada na exportação do HoC como casting para o tipo ComponentClass do react. Ex está acima na explicação de MyComponent.tsx.
        - MyComponent.scss : Arquivo sass do componente. Deve ser importado dentro do component como mostra o exemplo em MyComponent.tsx.
    
        Caso o componente seja muito complexo o mesmo poderá ser dividido em sub components que ficam dentro da pasta do component. Nesse caso o componente teria pastas de sub componentes dentro da pasta raíz e o component principal ficaria dentro de uma pasta com o mesmo nome da pasta raíz, Ex: 
            MyComponent
                    |
                    |--MyComponent
                        |
                        |-MyComponent.tsx
                        |
                        |-MyComponent.css
                        |
                        |-IMyComponent.css
                    |--MySubComponent
                        |
                        |-MySubComponent.tsx
                        |
                        |-MySubComponent.css
                        |
                        |-MySubComponent.css
                        
3. SCSS de um component
    * 3.1 O nome do arquivo SCSS deve ser o mesmo do componente principal e com extensão - scss, Ex: MyComponent.scss;
    * 3.2 A primeira classe deve ter o mesmo nome da classe principal
        ```javascript
        import React, { Component } from 'react';
        import "./MyComponent.scss";
        import { IMyComponent, MyComponentState, MyComponentExposedProps } from './IMyComponent';
        import { connect } from 'react-redux';
        export class MyComponent extends Component<IMyComponent, MyComponentState>{
            render(){
                return <div className="MyComponent">
                    <div>
                        <p>Conteudo</p>
                    </div>
                </div>
            }
        }
        ```
        - O arquivo SCSS deve ser criado apenas quando nescessario, em casos que o componente seja muito simples ou pequeno deve ser usado o modulo styled;
        - Endereço da documentação para estudo : https://www.styled-components.com/docs/basics;
            - No caso abaixo foi criado os componentes (Container, P), para exemplicar o uso do modulo styled:
             
            ```javascript
                import React, {Component} from 'react';
                import styled from 'styled-components';
                
                const P = styled("p")`
                    color: red !important;
                    font-size: 11px !important;
                `
                const Container = styled("div")`
                    width: 100%
                    height: 100%;
                `
                class ShowError extends Component{
                    render(): React.ReactNode {
                        return <Container>
                            <P>Conteudo</P>
                        </Container>
                    }
                }
            ```
            - Dentro do styled deve se aplicado a mesma sintaxe do SCSS (ex: :hover{color: blue}), o mesmo vale para o componente criado com o modulo (Ex: <Container/>) que tera  a mesma sintaxe de um elemento html;
                * Dentro do arquivo scss deve se respeitar os niveis hierárquicos, para manter o isolamento interno de estilo:
                    ```scss
                    .LabelWrapper{
                        width: 100%;
                        font-size: 13px;
                        .label-container{
                            display: flex;
                            align-items: center;
                            justify-content: flex-start;
                            position: relative;
                            height: 30px;
                            &:hover{
                                .help-tooltip{
                                    display: block;
                                }
                            }
                        }
                    }
                    ```
        * Variaveis de SCSS:
            - Por padrão já existe um arquivo scss (variables.scss)  
         |--src
            |
            |--variables.scss
                ```scss
                    //variables.scss
                    $color-1: #9ba6b3;
                ```
            - Importação de SCSS para arquivo SCSS;
            - Nesse caso e nescessario fazer um import do arquivo que possui as variaveis;
                - 
                ```scss
                @import "./../../../../../../variables";
                .menu-left-main {
                  width: 100%;
                  height: 100%;
               
                  .menu-integration {
                    color: $color-7; /* variavel */
                    background: $color-5 /* variavel */; 
                    margin: 0;
                    width: 40px;
                    height: 100%;
                   }
                   
               }
               ```